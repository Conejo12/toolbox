<?php

declare (strict_types = 1);

namespace Toolbox;

interface IRelation
{
    public function __construct($message);

    public function with($handler);

    public function supports($message);

    public function getHandler();

    public function sort();
}
