<?php

declare (strict_types = 1);

namespace Toolbox\Event;

interface IEventHandler
{
    public function handle(IEvent $event): void;

    public function supports(IEvent $event): bool;
}
