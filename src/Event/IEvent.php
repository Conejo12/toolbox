<?php

declare (strict_types=1);

namespace Toolbox\Event;

interface IEvent
{
    public function getEventId(): int;

    public function getEventName(): string;
}
