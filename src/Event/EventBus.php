<?php

declare(strict_types = 1);

namespace Toolbox\Event;

use Toolbox\Emitter\EventEmitter;
use Toolbox\Shared\Exception\HandlerNotFoundException;

class EventBus
{
    /** @var EventHandlerLocator */
    private $locator;

    /** @var EventEmitter */
    private $emitter;


    public function __construct(EventHandlerLocator $locator, EventEmitter $emitter)
    {
        $this->locator = $locator;
        $this->emitter = $emitter;
    }

    /**
     * @throws HandlerNotFoundException
     */
    public function dispatch(IEvent $event): void
    {
        $this->emitter->emit($event, $this->locator->getRelationFor($event));
    }
}
