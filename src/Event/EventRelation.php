<?php

declare (strict_types = 1);

namespace Toolbox\Event;

class EventRelation
{
    /** @var string */
    private $event;

    /** @var IEventHandler[] */
    private $handlers = [];


    public function __construct(string $event)
    {
        $this->event = $event;
    }

    public function append(IEventHandler $handler, int $priority): self
    {
        $this->handlers[$priority][] = $handler;

        return $this;
    }

    public function supports(string $event): bool
    {
        return $this->event === $event;
    }

    public function getHandlers(): iterable
    {
        $this->sortByPriority();

        foreach ($this->handlers as $priority => $handlers) {
            yield from $handlers;
        }
    }

    private function sortByPriority(): void
    {
        krsort($this->handlers, SORT_NUMERIC);
    }
}
