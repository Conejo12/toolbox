<?php

declare (strict_types = 1);

namespace Toolbox\Emitter;

use Toolbox\Event\EventRelation;
use Toolbox\Event\IEvent;

class EventEmitter
{
    public function emit(IEvent $event, EventRelation $relation): void
    {
        foreach ($relation->getHandlers() as $handler) {
            $handler->handle($event);
        }
    }
}
