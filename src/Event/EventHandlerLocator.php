<?php

declare (strict_types = 1);

namespace Toolbox\Event;

use Toolbox\Shared\Exception\HandlerNotFoundException;
use Toolbox\Shared\Exception\TooManyHandlersException;

class EventHandlerLocator
{
    /** @var EventRelation[] */
    private $relations;

    /**
     * @throws HandlerNotFoundException
     */
    public function getRelationFor(IEvent $event): EventRelation
    {
        foreach ($this->relations as $relation) {
            if ($relation->supports($event->getEventName())) {
                return $relation;
            }
        }

        throw HandlerNotFoundException::forEvent($event);
    }

    /**
     * @throws TooManyHandlersException
     */
    public function relate(IEventHandler $handler, string $eventName, int $priority): void
    {
        foreach ($this->relations as $relation) {
            if ($relation->supports($eventName)) {
                $relation->append($handler, $priority);
                return;
            }
        }

        $relation = new EventRelation($eventName);
        $relation->append($handler, $priority);

        $this->relations[] = $relation;
    }
}
