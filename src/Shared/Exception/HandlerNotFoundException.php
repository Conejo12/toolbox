<?php

declare (strict_types = 1);

namespace Toolbox\Shared\Exception;

use Toolbox\Command\ICommand;
use Exception;
use Toolbox\Event\IEvent;

class HandlerNotFoundException extends Exception
{
    public static function forCommand(ICommand $command): self
    {
        $message = sprintf('Cannot find handler for command "%s"', $command->getCommandName());

        return new self($message);
    }

    public static function forEvent(IEvent $event): self
    {
        $message = sprintf('Cannot find any handler for event "%s"', $event->getEventName());

        return new self($message);
    }
}
