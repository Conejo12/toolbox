<?php

declare (strict_types = 1);

namespace Toolbox\Shared\Exception;

use Exception;

class TooManyHandlersException extends Exception
{
    public static function forCommandName(string $commandName): self
    {
        $message = sprintf('It cannot be more than one handler for command "%s"', $commandName);

        return new self($message);
    }
}
