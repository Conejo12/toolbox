<?php

declare (strict_types = 1);

namespace Toolbox\Command;

class CommandRelation
{
    /** @var string */
    private $command;

    /** @var ICommandHandler */
    private $handler;


    public function __construct(string $command)
    {
        $this->command = $command;
    }

    public function append(ICommandHandler $handler): void
    {
        $this->handler = $handler;
    }

    public function supports(string $command): bool
    {
        return $this->command === $command;
    }

    public function getHandler(): ICommandHandler
    {
        return $this->handler;
    }
}
