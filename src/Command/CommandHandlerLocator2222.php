<?php

declare (strict_types = 1);

namespace Toolbox\Command;

use Toolbox\Shared\Exception\HandlerNotFoundException;
use Toolbox\Shared\Exception\TooManyHandlersException;

class CommandHandlerLocator
{
    /** @var array */
    private $relations = [];

    /**
     * @throws HandlerNotFoundException
     */
    public function getHandlerFor(ICommand $command): ICommandHandler
    {
        if (!isset($this->relations[$command->getCommandName()])) {
            throw HandlerNotFoundException::forCommand($command->getCommandName());
        }

        return $this->relations[$command->getCommandName()];
    }

    /**
     * @throws TooManyHandlersException
     */
    public function relate(ICommandHandler $handler, string $commandName): void
    {
        if (array_key_exists($commandName)) {
            throw TooManyHandlersException::forCommand($commandName);
        }

        $this->relations[$commandName] = $handler;
    }
}
