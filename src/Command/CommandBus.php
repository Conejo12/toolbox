<?php

declare(strict_types = 1);

namespace Toolbox\Command;

use Toolbox\Emitter\CommandEmitter;
use Toolbox\Shared\Exception\HandlerNotFoundException;

class CommandBus
{
    /** @var CommandHandlerLocator */
    private $locator;

    /** @var CommandEmitter */
    private $emitter;


    public function __construct(CommandHandlerLocator $locator, CommandEmitter $emitter)
    {
        $this->locator = $locator;
        $this->emitter = $emitter;
    }

    /**
     * @throws HandlerNotFoundException
     */
    public function dispatch(ICommand $command): void
    {
        $this->emitter->emit($command, $this->locator->getRelationFor($command));
    }
}
