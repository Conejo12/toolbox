<?php

declare (strict_types = 1);

namespace Toolbox\Command;

interface ICommandHandler
{
    public function handle(ICommand $command): void;

    public function supports(ICommand $command): bool;
}
