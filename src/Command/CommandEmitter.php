<?php

declare (strict_types = 1);

namespace Toolbox\Emitter;

use Toolbox\Command\CommandRelation;
use Toolbox\Command\ICommand;

class CommandEmitter
{
    public function emit(ICommand $command, CommandRelation $relation): void
    {
        $relation->getHandler()->handle($command);
    }
}
