<?php

declare (strict_types=1);

namespace Toolbox\Command;

use Toolbox\IMessage;

interface ICommand extends IMessage
{
    public function getCommandId(): int;

    public function getCommandName(): string;
}
