<?php

declare (strict_types = 1);

namespace Toolbox\Command;

use Toolbox\Shared\Exception\HandlerNotFoundException;
use Toolbox\Shared\Exception\TooManyHandlersException;

class CommandHandlerLocator
{
    /** @var CommandRelation[] */
    private $relations;

    /**
     * @throws HandlerNotFoundException
     */
    public function getRelationFor(ICommand $command): CommandRelation
    {
        foreach ($this->relations as $relation) {
            if ($relation->supports($command->getCommandName())) {
                return $relation;
            }
        }

        throw HandlerNotFoundException::forCommand($command);
    }

    /**
     * @throws TooManyHandlersException
     */
    public function relate(ICommandHandler $handler, string $commandName): void
    {
        foreach ($this->relations as $relation) {
            if ($relation->supports($commandName)) {
                throw TooManyHandlersException::forCommandName($commandName);
            }
        }

        $relation = new CommandRelation($commandName);
        $relation->append($handler);

        $this->relations[] = $relation;
    }
}
