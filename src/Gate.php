<?php

declare(strict_types=1);

namespace Toolbox;

class Gate
{
    /** @var IBus */
    private $dispatcher;

    /** @var IDispatchHistory */
    private $dispatchHistory;

    public function __construct(IBus $dispatcher, IDispatchHistory $dispatchHistory)
    {
        $this->dispatcher = $dispatcher;
        $this->dispatchHistory = $dispatchHistory;
    }

    public function dispatch(IMessage $message): void
    {
        $this->dispatchHistory->refresh($message);

        if (!$this->dispatchHistory->exists($message)) {
            $this->dispatcher->dispatch($message);
            $this->dispatchHistory->register($message);
        }
    }
}