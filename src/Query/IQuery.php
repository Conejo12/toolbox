<?php

declare (strict_types = 1);

namespace Toolbox\Query;

use Toolbox\IMessage;

interface IQuery extends IMessage
{

}