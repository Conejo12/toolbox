<?php

declare(strict_types=1);

namespace Toolbox;

interface IDispatchHistory
{
    public function register(IMessage $message): void;

    public function exists(IMessage $message): bool;

    public function refresh(IMessage $message): void;
}